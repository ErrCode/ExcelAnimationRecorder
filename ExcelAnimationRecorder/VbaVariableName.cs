﻿using System;
using System.Linq;
using System.Text;

namespace ExcelAnimationRecorder
{
    static class VbaVariableName
    {
        /*
        http://excelvbatutor.com/vba_chp2.htm

            They must not exceed 40 characters
            They must contain only letters, numbers and underscore chacaters
            No spacing is allowed
            It must not begin with a number
            Period is not permitted
        */
        private static char[] _allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_".ToCharArray();
        private static char[] _disallowedInitialChars = "0123456789_".ToCharArray();

        public static void ValidateVariableName(string input)
        {
            if (input.Length < 1)
                throw new Exception($"The variable name {input} is shorter than 1 character");

            if (input.Length > 40)
                throw new Exception($"The variable name {input} is longer than 40 characters");

            if (input.Any(c => !_allowedChars.Contains(c)))
                throw new Exception($"The variable name {input} contains a character other than: {Environment.NewLine} {new string(_allowedChars)}");

            if (_disallowedInitialChars.Contains(input[0]))
                throw new Exception($"The variable name {input} starts with a disallowed character ({new string(_disallowedInitialChars)})");

        }

        public static string GenerateVariableName(string input)
        {
            var name = input.Replace(" ", "");

            var newName = new StringBuilder();
            for (int i = 0; i < name.Length; i++)
            {
                var letter = name[i];
                if (_allowedChars.Contains(letter))
                    newName.Append(letter);
            }

            char c;
            do
            {
                c = newName[0];

                if (_disallowedInitialChars.Contains(c))
                    newName.Remove(0, 1);
                else
                    break;
            }
            while (true);

            newName[0] = char.ToLower(newName[0]);

            name = newName.ToString();

            if (name.Length > 40)
                name = name.Substring(0, 40);

            return name;
        }
    }
}