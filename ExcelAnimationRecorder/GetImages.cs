﻿using System;
using System.Runtime.InteropServices;
using NetOffice.Tools;
using NetOffice.ExcelApi.Tools;
using Excel = NetOffice.ExcelApi;
using Office = NetOffice.OfficeApi;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;

namespace ExcelAnimationRecorder
{
    public partial class Addin : COMAddin
    {
        private Dictionary<string, Bitmap> _images = new Dictionary<string, Bitmap>();

        public Bitmap GetImage(Office.IRibbonControl control)
        {
            return _images.ContainsKey(control.Id) ? _images[control.Id] : LoadFirstTime(control.Id);
        }

        private Bitmap LoadFirstTime(string controlId)
        {
            var resourceName = Assembly.GetExecutingAssembly()
                .GetManifestResourceNames()
                .FirstOrDefault(n => n.Contains("." + controlId + "."));

            var bitmap = resourceName == null ? null : new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName));

            _images.Add(controlId, bitmap);

            return bitmap;
        }
    }
}
