﻿using System;
using NetOffice.ExcelApi.Tools;
using Office = NetOffice.OfficeApi;

namespace ExcelAnimationRecorder
{
    public partial class Addin : COMAddin
    {
        public string GetSupertip(Office.IRibbonControl control)
        {
            switch (control.Id)
            {
                case "WriteShapeReferences":
                    return "Click this button to generate code that sets up local variables for all watched shapes on this sheet";

                case "WritePropertyWatchers":
                    return "Click this button to generate code that sets all watched properties to their current values";

                case "ShapeDropDown":
                    return "Select a shape whose watch settings you wish to change";

                case "VariableNameEditBox":
                    return "Edit the local variable name for this shape";

                case "PropertiesComboBox":
                    return "Add a watched property or nested property by typing it and pressing enter. " + Environment.NewLine
                        + Environment.NewLine
                        + "Remove a watched property or nested property by selecting it in the dropdown.";
            }

            return "";
        }
    }
}
