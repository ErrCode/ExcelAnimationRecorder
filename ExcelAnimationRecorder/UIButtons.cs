﻿using System;
using NetOffice.ExcelApi.Tools;
using Office = NetOffice.OfficeApi;
using System.Linq;
using System.Windows.Forms;

namespace ExcelAnimationRecorder
{
    public partial class Addin : COMAddin
    {
        protected override void TaskPaneVisibleStateChanged(Office._CustomTaskPane customTaskPaneInst)
        {
            RibbonUI?.InvalidateControl("YourCodeToggleButton");
        }

        public bool OnGetPressedPanelToggle(Office.IRibbonControl control)
        {
            return TaskPanes[0]?.Visible ?? false;
        }

        public void OnCheckPanelToggle(Office.IRibbonControl control, bool pressed)
        {
            if (TaskPanes.Count > 0)
                TaskPanes[0].Visible = pressed;
        }

        public bool WritePropertyWatchers_GetEnabled(Office.IRibbonControl control)
        {
            return PropertyWatchers.FirstOrDefault(w => !string.IsNullOrWhiteSpace(w.ShapeName)) != null;
        }
        public bool WriteShapeReferences_GetEnabled(Office.IRibbonControl control)
        {
            return PropertyWatchers.FirstOrDefault(w => !string.IsNullOrWhiteSpace(w.ShapeName)) != null;
        }        

        public void OnClickWriteShapeReferences(Office.IRibbonControl control)
        {
            try
            {
                var yourcode = TaskPaneInstances.FirstOfType<YourCode>();
                yourcode.WriteShapeReferences(PropertyWatchers);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        
        public void OnClickWritePropertyWatchers(Office.IRibbonControl control)
        {
            try
            {
                var yourcode = TaskPaneInstances.FirstOfType<YourCode>();
                yourcode.WritePropertyWatchers(PropertyWatchers);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        public void SelectPropertyButton_OnAction(Office.IRibbonControl control)
        {
            var shape = GetPropertyWatcher().Sheet.Shapes[GetPropertyWatcher().ShapeName];
            var selector = new PropertySelector(shape, _excelVersion);




            selector.ShowDialog();
            var prop = selector.SelectedProperty;

            if (prop != null)
                PropertiesComboBox_OnChange(null, selector.SelectedProperty);
        }

        private void RefreshButtons()
        {
            RibbonUI.InvalidateControl("WritePropertyWatchers");
            RibbonUI.InvalidateControl("WriteShapeReferences");
            RibbonUI.InvalidateControl("SelectPropertyButton");
        }
    }
}
