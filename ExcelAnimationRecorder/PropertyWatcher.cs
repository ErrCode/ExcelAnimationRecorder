﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Excel = NetOffice.ExcelApi;

namespace ExcelAnimationRecorder
{
    public class PropertyWatcher
    {
        private string _variableName = string.Empty;
        public Excel.Worksheet Sheet { get; }
        public string ShapeName { get; set; }
        public List<string> PropertyNames { get; } = new List<string>();

        public PropertyWatcher(Excel.Shape shape, Excel.Worksheet sheet)
        {
            ShapeName = shape.Name;
            Sheet = sheet;
        }

        public PropertyWatcher(string shapeName, Excel.Worksheet sheet)
        {
            ShapeName = shapeName;
            Sheet = sheet;
        }

        public static PropertyWatcher Empty { get; } = new PropertyWatcher();

        private PropertyWatcher()
        {
        }

        public string VariableName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_variableName))
                    GenerateVariableName();

                return _variableName;
            }
            set
            {
                try {
                    VbaVariableName.ValidateVariableName(value);
                    _variableName = value;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    GenerateVariableName();
                }

            }
        }

        private void GenerateVariableName()
        {
            _variableName = string.IsNullOrWhiteSpace(ShapeName) ? ShapeName : VbaVariableName.GenerateVariableName(ShapeName);
        }
    }

  
}