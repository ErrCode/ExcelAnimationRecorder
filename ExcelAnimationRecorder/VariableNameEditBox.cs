﻿using NetOffice.ExcelApi.Tools;
using Office = NetOffice.OfficeApi;
using System.Windows.Forms;
using System;

namespace ExcelAnimationRecorder
{
    public partial class Addin : COMAddin
    {
        public bool VariableNameEditBox_GetEnabled(Office.IRibbonControl control) {
            return !string.IsNullOrWhiteSpace(ShapeDropDown_Selected);
        }

        public string VariableNameEditBox_GetText(Office.IRibbonControl control) {
            try {
                return GetPropertyWatcher().VariableName;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{nameof(VariableNameEditBox_GetText)}:{Environment.NewLine}{ex.ToString()}");
                return "";
            }
        }
        public void VariableNameEditBox_OnChange(Office.IRibbonControl control, string text)
        {
            try {
                GetPropertyWatcher().VariableName = text;

                RefreshVariableNameEditBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{nameof(VariableNameEditBox_OnChange)}:{Environment.NewLine}{ex.ToString()}");
            }
        }

        private void RefreshVariableNameEditBox()
        {
            RibbonUI.InvalidateControl("VariableNameEditBox");
        }
    }
}
