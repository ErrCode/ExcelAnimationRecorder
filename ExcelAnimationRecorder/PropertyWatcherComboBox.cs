﻿using NetOffice.ExcelApi.Tools;
using Office = NetOffice.OfficeApi;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System;

namespace ExcelAnimationRecorder
{
    public partial class Addin : COMAddin
    {
        public List<PropertyWatcher> PropertyWatchers { get; } = new List<PropertyWatcher>();

        public PropertyWatcher GetPropertyWatcher() {

            var propertyWatcher = PropertyWatchers.Where(p => p.Sheet == _app.GetActiveSheet())
                .FirstOrDefault(wp => wp.ShapeName == ShapeDropDown_Selected);

            if (propertyWatcher == null)
            {
                propertyWatcher = new PropertyWatcher(ShapeDropDown_Selected, _app.GetActiveSheet());

                if (!string.IsNullOrWhiteSpace(ShapeDropDown_Selected))
                    PropertyWatchers.Add(propertyWatcher);
            }

            RefreshButtons();

            return propertyWatcher;
        }


        public bool PropertiesComboBox_GetEnabled(Office.IRibbonControl control)
        {
            return !string.IsNullOrWhiteSpace(ShapeDropDown_Selected);
        }

        public int PropertiesComboBox_GetItemCount(Office.IRibbonControl control)
        {
            return GetPropertyWatcher().PropertyNames.Count;
        }

        public string PropertiesComboBox_GetItemLabel(Office.IRibbonControl control, int index)
        {
            return GetPropertyWatcher().PropertyNames[index];
        }

        public void PropertiesComboBox_OnChange(Office.IRibbonControl control, string text)
        {
            text = text.Trim();

            if (string.IsNullOrEmpty(ShapeDropDown_Selected))
                return;

            var propertyWatcher = GetPropertyWatcher();

            if (!propertyWatcher.PropertyNames.Contains(text))
            {
                try
                {
                    var propChain = text.Split('.').ToList();

                    var ret = PropertySupport.ValidatePropertyChain(propChain, _excelVersion);

                    propertyWatcher.PropertyNames.Add(string.Join(".", ret));
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{text} isn't a supported property{Environment.NewLine}{Environment.NewLine}{ex.Message}");
                }

            }
            else
            {
                propertyWatcher.PropertyNames.Remove(text);
            }

            RefreshPropertiesComboBox();
        }

        private void RefreshPropertiesComboBox()
        {
            RibbonUI.InvalidateControl("PropertiesComboBox");
        }
    }
}
