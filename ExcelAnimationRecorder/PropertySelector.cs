﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Excel = NetOffice.ExcelApi;

namespace ExcelAnimationRecorder
{
    public partial class PropertySelector : Form
    {
        private Type[] _excludedTypes = new[] {
            typeof(object),
            typeof(Type),
            typeof(NetOffice.COMObject),
            typeof(NetOffice.ICOMObject),
        };

        private PropertyInfo[] _excludedProperties;

        private object BaseObject { get; set; }
        private double ExcelVersion { get; set; }
        public string SelectedProperty { get; private set; }


        private PropertySelector()
        {
            InitializeComponent();

            var comObjectType = typeof(NetOffice.COMObject);
            _excludedProperties = comObjectType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        }

        public PropertySelector(Excel.Shape shape, double excelVersion) : this()
        {
            BaseObject = shape;
            ExcelVersion = excelVersion;
        }

        private void PropertySelector_Load(object sender, EventArgs e)
        {
            var type = BaseObject.GetType();
            var baseNode = new TreeNode($"{type.GetProperty("Name").GetValue(BaseObject, null)} ({type.Name})");
            treeView.Nodes.Add(baseNode);
            AddProperties(type, baseNode);
            baseNode.Expand();
            treeView.NodeMouseClick += TreeView_NodeMouseClick;
        }

        private void TreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                var node = e.Node;

                if (node.Level == 0)
                    return;

                //if (node.Nodes.Count > 0)
                //    return;

                var propInfo = (PropertyInfo)node.Tag;

                //Clear placeholders
                for (int i = node.Nodes.Count - 1; i >= 0; i--)
                {
                    if (node.Nodes[i] is EmptyTreeNode)
                        node.Nodes.RemoveAt(i);

                }

                AddProperties(propInfo.PropertyType, node);
            }
            catch (Exception ex)
            {
                MessageBox.Show(nameof(TreeView_NodeMouseClick) + ":" + Environment.NewLine + ex.ToString());
            }
        }

        private void AddProperties(Type type, TreeNode baseNode)
        {
            try {
                var props = PropertySupport.GetSupportedProperties(type, ExcelVersion)
                    .Where(p => !IsExcludedProp(p))
                    .ToList();

                foreach (var prop in props)
                {
                    var propType = prop.PropertyType;
                    var node = new TreeNode($"{prop.Name} ({propType.Name})");
                    node.Tag = prop;
                    baseNode.Nodes.Add(node);

                    try
                    {   //If supported property
                        PropertySupport.ThrowIfNotSupportedProperty(prop);
                        node.BackColor = System.Drawing.Color.Green;
                    }
                    catch (Exception ex) { //else
                        if (ex.Message.Contains("is not a primitive type (including strings and enums"))
                        {
                            node.Nodes.Add(new EmptyTreeNode());
                        }

                        //else if (ex.Message.Contains(" is read-only"))
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(nameof(AddProperties) + ":" +  Environment.NewLine + ex.ToString());
            }
        }

        private bool IsExcludedProp(PropertyInfo propertyInfo)
        {
            if (_excludedProperties.Contains(propertyInfo))
                return true;

            if (_excludedTypes.Contains(propertyInfo.PropertyType))
                return true;

            return false;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            var node = treeView.SelectedNode;

            SelectedProperty = GetFullPath(node);

            this.Close();
        }

        private string GetFullPath(TreeNode node)
        {
            var path = new StringBuilder();

            while (node.Level > 0)
            {
                var propInfo = (PropertyInfo)node.Tag;
                path.Insert(0, "." + propInfo.Name);

                node = node.Parent;
            }

            path.Remove(0, 1);

            return path.ToString();
        }
    }
}
