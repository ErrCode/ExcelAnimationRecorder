﻿using System;
using NetOffice.ExcelApi.Tools;
using System.Collections.Generic;

namespace ExcelAnimationRecorder
{
    public static class ITaskPaneEx
    {
        public static T FirstOfType<T>(this IEnumerable<ITaskPane> panes)
        {
            foreach (var pane in panes)
            {
                if (pane is T)
                    return (T)pane;
            }

            throw new Exception($"Couldnt find pane of type {typeof(T).FullName}");
        }
    }
}