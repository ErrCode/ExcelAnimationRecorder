﻿using System;
using System.Collections;
using System.Collections.Generic;
using IO=System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WixSharp;

namespace ExcelAnimationRecorderInstaller
{
    class Program
    {
        private const string _addinOutputFolder = @"C:\johan\ExcelAnimationRecorder\ExcelAnimationRecorder\bin\Release";
        private const string _installPath = @"%LocalAppDataFolder%\JohanB\ExcelAnimationRecorder";
        private const string _wixPath = @"C:\johan\ExcelAnimationRecorder\packages\WixSharp.wix.bin.3.10.1\tools\bin";

        static void Main(string[] args)
        {
            Compiler.WixLocation = _wixPath;
            Compiler.PreserveTempFiles = true;
            
            var files = GetFiles().ToArray();
            var baseDir = new Dir(_installPath, files);
            
            var project = new Project("Excel Animation Recorder", baseDir, new Property("REINSTALLMODE", "amus"));

            project.MajorUpgradeStrategy = MajorUpgradeStrategy.Default;
            project.MajorUpgradeStrategy.RemoveExistingProductAfter = Step.InstallInitialize;
            project.Version = GetVersion();

            project.ControlPanelInfo.ProductIcon = "1482955214_player_record.ico";
            project.ControlPanelInfo.Contact = "Johan Bjurstam (johan.bjurstam@gmail.com)";

            project.LicenceFile = IO.Path.GetFullPath("License Agreement.rtf");

            project.GUID = new Guid("6f330b47-2577-43ad-9095-1861ba25889b");
            project.Id = "ExcelAnimationRecorder";
            project.UI = WUI.WixUI_Minimal;
            project.OutFileName = "ExcelAnimationRecorder";
            project.Platform = Platform.x86;

            var fileArg = ", DLLFILES=" + string.Join(";", files.Select(f => f.Name).Where(n => IO.Path.GetExtension(n).Equals(".dll", StringComparison.InvariantCultureIgnoreCase)).Select(p => "[INSTALLDIR]" + IO.Path.GetFileName(p))); 
            
            var installAction = new ElevatedManagedAction(CustomActions.Install, Return.check, When.Before, Step.InstallFinalize, Condition.NOT_BeingRemoved)// + "AND NOT REINSTALL")
            {
                UsesProperties = "PLATFORM=x86" + fileArg
            };
            var uninstallAction = new ElevatedManagedAction(CustomActions.Uninstall, Return.check, When.Before, Step.RemoveFiles, Condition.BeingRemoved) {
                UsesProperties = "PLATFORM=x86" + fileArg
            };
      

            project.Actions = new[] { installAction, uninstallAction };

            var file = Compiler.BuildMsi(project);
            Console.WriteLine("Built {0}", file);

            project.Platform = Platform.x64;
            project.OutFileName += "x64";
            installAction.UsesProperties = "PLATFORM=x64" + fileArg;
            uninstallAction.UsesProperties = "PLATFORM=x64" + fileArg;

            file = Compiler.BuildMsi(project);
            Console.WriteLine("Built {0}", file);

            Console.ReadLine();
        }

        private static Version GetVersion()
        {
            var version = "1.0.0.0";

            if (IO.File.Exists("version"))
            {
                var readVersion = IO.File.ReadAllText("version").Trim();
                var splits = readVersion.Split('.');

                if (splits.Length == 4)
                    version = readVersion;
            }

            var versionParts = version.Split('.').Select(s => int.Parse(s)).ToList();

            versionParts[3]++;

            version = string.Join(".", versionParts);

            IO.File.WriteAllText("version", version);

            return new Version(version);
        }

        private static IEnumerable<File> GetFiles()
        {
            var sourceDir = new IO.DirectoryInfo(_addinOutputFolder);
            foreach (var file in sourceDir.GetFiles())
            {
                Console.WriteLine("Adding {0} ...", file.FullName);
                yield return new File(file.FullName) {
                    
                };
            }
        }
    }
}

/*
        With special handler for reinstall:

        var installAction = new ElevatedManagedAction(CustomActions.Install, Return.check, When.Before, Step.InstallFinalize, Condition.NOT_BeingRemoved)// + "AND NOT REINSTALL")
            {
                UsesProperties = "PLATFORM=x86" + fileArg
            };
            var uninstallAction = new ElevatedManagedAction(CustomActions.Uninstall, Return.check, When.Before, Step.RemoveFiles, Condition.BeingRemoved) {
                UsesProperties = "PLATFORM=x86" + fileArg
            };
            //var reinstallAction = new ElevatedManagedAction(CustomActions.Reinstall, Return.check, When.Before, Step.InstallFinalize, "REINSTALL") {
            //    UsesProperties = "PLATFORM=x86" + fileArg
            //};
*/
